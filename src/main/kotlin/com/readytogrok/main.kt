package com.readytogrok

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Frank on 10/3/2017.
 */

fun main(args: Array<String>) {

    /*
    * RxJava 2 Walk-through Lesson 9
    * Subtitle: Multiple observeOn operators
    *
    * In lesson 8, we saw how we could use the observeOn() operator to make the
    * subscriber run on a different thread than the Observable.
    *
    * In this lesson, we build on that a bit, to show what happens when we
    * have multiple observeOn() operators in our stream.
    *
    * */

    println("Program has started on thread: " + Thread.currentThread().name)

    val obs = Observable.create<String> { e ->
        println("Emitting items on thread: " + Thread.currentThread().name)

        val quote = "Way out west there was this fella... fella I wanna tell ya about. Fella by the name of " +
                "Jeff Lebowski. At least that was the handle his loving parents gave him, but he never had " +
                "much use for it himself. Mr. Lebowski, he called himself \"The Dude\". Now, \"Dude\" - that's " +
                "a name no one would self-apply where I come from. But then there was a lot about the Dude " +
                "that didn't make a whole lot of sense. And a lot about where he lived, likewise. But then " +
                "again, maybe that's why I found the place so darned interestin'. They call Los Angeles the " +
                "\"City Of Angels.\" I didn't find it to be that, exactly. But I'll allow there are some nice " +
                "folks there. 'Course I can't say I've seen London, and I ain't never been to France. And I " +
                "ain't never seen no queen in her damned undies, so the feller says. But I'll tell you what - " +
                "after seeing Los Angeles, and this here story I'm about to unfold, well, I guess I seen " +
                "somethin' every bit as stupefyin' as you'd see in any of them other places. And in English, " +
                "too. So I can die with a smile on my face, without feelin' like the good Lord gypped me. Now " +
                "this here story I'm about to unfold took place back in the early '90s - just about the time " +
                "of our conflict with Sad'm and the I-raqis. I only mention it because sometimes there's a " +
                "man... I won't say a hero, 'cause, what's a hero? But sometimes, there's a man. And I'm " +
                "talkin' about the Dude here. Sometimes, there's a man, well, he's the man for his time and " +
                "place. He fits right in there. And that's the Dude, in Los Angeles. And even if he's a lazy " +
                "man - and the Dude was most certainly that. Quite possibly the laziest in Los Angeles County, " +
                "which would place him high in the runnin' for laziest worldwide. But sometimes there's a man, " +
                "sometimes, there's a man. Aw. I lost my train of thought here. But... aw, hell. I've done " +
                "introduced him enough."

        quote.split(" ", ",", ".", "\"")
                .forEach {
                    Thread.sleep(20)
                    e.onNext(it)
                }

        e.onComplete()
    }
            .filter({ !it.isEmpty() })
            .subscribeOn(Schedulers.computation())

    obs.observeOn(Schedulers.newThread()) // first call to observeOn
            .doOnNext({ println("doOnNext called on thread: " + Thread.currentThread().name)})
            .observeOn(Schedulers.newThread()) // second call to observeOn
            .subscribe(
                    { println("onNext called on thread: " + Thread.currentThread().name + ", with value: " + it) },
                    { println("onError called on thread: " + Thread.currentThread().name + ", with message: " + it.message) },
                    { println("onComplete called on thread: " + Thread.currentThread().name) })

    Thread.sleep(10000)

    println("Ending program on thread: " + Thread.currentThread().name)

    /*
    *
    * Run this program and examine the output. You should see these three things:
    *
    * 1. The items are emitted on a computation thread.
    *
    * 2. For each emitted string, there is a log message that looks like:
    *
    *       doOnNext called on thread: <thread_name>
    *
    *    where <thread_name> is usually a name like RxNewThreadScheduler-1
    *
    * 3. The subscriber receives the items on yet ANOTHER thread, with
    *    a name like RxNewThreadScheduler-2
    *
    * Each call to observeOn() affects all of the operators downstream between
    * itself and the next call to observeOn(). This includes any subscribers,
    * where applicable.
    *
    * However, there is no sense in making multiple subscribeOn() calls. The
    * subscribeOn() operator only affects the thread on which the Observable
    * itself does its work.
    *
    * You can go ahead and add another call to subscribeOn() in the stream, if
    * you want to. Only the first call will have any effect.
    *
    * */

    /*
    *
    * Once you are ready to move on, discard any local changes you've made (if necessary):
    *
    *     git reset --hard HEAD
    *
    * and then move on to Lesson 10:
    *
    *     git checkout lesson_10
    *
    * */
}